# Translation of ejabberd debconf templates to French
# Copyright (C) 2008 Florentin Duneau <fduneau@gmail.com>
# This file is distributed under the same license as the ejabberd package.
#
# Florentin Duneau <f.baced@wanadoo.fr>, 2006.
# Florentin Duneau <fduneau@gmail.com>, 2008.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2015, 2017.
#
# Note to translators:
# Please do not translate the variables ${hostname}, ${user}, ${preseed}, and
# any other which may appear in the future. Changes to these variables will
# break the scripts. Thank you!
msgid ""
msgstr ""
"Project-Id-Version: ejabberd\n"
"Report-Msgid-Bugs-To: ejabberd@packages.debian.org\n"
"POT-Creation-Date: 2017-12-06 08:27+0100\n"
"PO-Revision-Date: 2017-11-28 23:31+0100\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. Type: string
#. Description
#: ../templates:2001
msgid "Hostname for this Jabber server:"
msgstr "Nom d'hôte du serveur Jabber :"

#. Type: string
#. Description
#: ../templates:2001
msgid "Please enter a hostname for this Jabber server."
msgstr "Veuillez indiquer un nom d'hôte pour le serveur Jabber."

#. Type: string
#. Description
#: ../templates:2001
msgid ""
"If you would like to configure multiple hostnames for this server, you will "
"have to do so manually in /etc/ejabberd/ejabberd.yml after installation."
msgstr ""
"Si vous souhaitez configurer plusieurs noms d'hôtes pour ce serveur, vous "
"devrez le faire manuellement dans /etc/ejabberd/ejabberd.yml après "
"l'installation."

# Note to translators:
# Please do not translate the variables ${hostname}, ${user}, ${preseed}, and
# any other which may appear in the future. Changes to these variables will
# break the scripts. Thank you!
#. Type: string
#. Description
#: ../templates:3001
msgid "Jabber server administrator username:"
msgstr "Identifiant de l'administrateur du serveur Jabber :"

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Please provide the name of an account to administrate the ejabberd server. "
"After the installation of ejabberd, you can log in to this account using "
"either any Jabber client or a web browser pointed at the administrative "
"https://${hostname}:5280/admin/ interface."
msgstr ""
"Veuillez indiquer un identifiant afin d'administrer le serveur ejabberd. "
"Après l'installation, vous pouvez vous connecter à ce compte avec tout "
"client Jabber ou un navigateur web pointant sur l'interface d'administration "
"https://${hostname}:5280/admin/."

# Note to translators:
# Please do not translate the variables ${hostname}, ${user}, ${preseed}, and
# any other which may appear in the future. Changes to these variables will
# break the scripts. Thank you!
#. Type: string
#. Description
#: ../templates:3001
msgid ""
"You only need to enter the username part here (such as ${user}), but the "
"full Jabber ID (such as ${user}@${hostname}) is required to access the "
"ejabberd web interface."
msgstr ""
"Veuillez seulement entrer ici l'identifiant (par exemple, « ${user} »). "
"Cependant, vous devrez utiliser une identité Jabber complète (par exemple, "
"« ${user}@${hostname} ») pour vous connecter à l'interface web."

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Please leave this field empty if you don't want to create an administrator "
"account automatically."
msgstr ""
"Veuillez laisser ce champ vide si vous ne souhaitez pas créer de compte "
"administrateur automatiquement."

#. Type: password
#. Description
#: ../templates:4001
msgid "Jabber server administrator password:"
msgstr "Mot de passe de l'administrateur du serveur Jabber :"

#. Type: password
#. Description
#: ../templates:4001
msgid "Please enter the password for the administrative user."
msgstr "Veuillez choisir le mot de passe de l'utilisateur administrateur."

#. Type: password
#. Description
#: ../templates:5001
msgid "Re-enter password to verify:"
msgstr "Entrez à nouveau le mot de passe pour vérification :"

#. Type: password
#. Description
#: ../templates:5001
msgid ""
"Please enter the same administrator password again to verify that you have "
"typed it correctly."
msgstr ""
"Veuillez entrer à nouveau le mot de passe administrateur pour vérifier que "
"vous l'avez entré correctement."

#. Type: error
#. Description
#: ../templates:6001
msgid "Password input error"
msgstr "Erreur d'entrée du mot de passe"

#. Type: error
#. Description
#: ../templates:6001
msgid ""
"The two passwords you entered did not match or were empty. Please try again."
msgstr ""
"Les deux mots de passe saisis ne correspondent pas ou sont vides. Veuillez "
"recommencer."

#. Type: error
#. Description
#: ../templates:7001
msgid "Invalid administrator account username"
msgstr "Identifiant du compte administrateur non valable"

#. Type: error
#. Description
#: ../templates:7001
msgid ""
"The username previously specified contains forbidden characters. Please "
"respect the JID syntax (https://tools.ietf.org/html/rfc6122#appendix-A.5). "
"If you used a full JID (e.g. user@hostname), the hostname needs to match the "
"one previously specified."
msgstr ""
"L'identifiant que vous venez d'entrer contient des caractères interdits. "
"Veuillez respecter la syntaxe de l'identité Jabber (JID) (http://tools.ietf."
"org/html/rfc6122#appendix-A.5). Si vous utilisez une identité Jabber "
"complète (par exemple, user@hostname), le nom d'hôte doit correspondre à "
"celui que vous avez entré précédemment."

#. Type: error
#. Description
#: ../templates:8001
msgid "Invalid hostname"
msgstr "Nom d'hôte non valable"

#. Type: error
#. Description
#: ../templates:8001
msgid ""
"The hostname previously specified contains forbidden characters or is "
"otherwise invalid. Please correct it and try again."
msgstr ""
"Le nom d'hôte précédemment entré contient des caractères interdits ou n'est "
"pas valable pour d'autres raisons. Veuillez le corriger et recommencer."

#. Type: error
#. Description
#: ../templates:9001
msgid "Invalid preseeded configuration"
msgstr "Préconfiguration non valable"

# Note to translators:
# Please do not translate the variables ${hostname}, ${user}, ${preseed}, and
# any other which may appear in the future. Changes to these variables will
# break the scripts. Thank you!
#. Type: error
#. Description
#: ../templates:9001
msgid ""
"A newer ${preseed} validation is being used and has determined that the "
"currently setup ${preseed} is invalid or incorrectly specified."
msgstr ""
"Une validation de ${preseed} plus récente est utilisée et a déterminé que le "
"${preseed} actuellement configuré n'est pas valable ou pas correctement "
"indiqué."

#. Type: error
#. Description
#: ../templates:9001
msgid ""
"If you would like to correct it, please backup your data and run dpkg-"
"reconfigure ejabberd after the upgrade is finished and note that any "
"databases and usernames will be lost or invalidated in this process if the "
"hostname is changed."
msgstr ""
"Si vous souhaitez le corriger, veuillez sauvegarder vos données et exécuter "
"dpkg-reconfigure ejabberd après la fin de la mise à niveau, et notez que "
"tous les noms d'utilisateur et les bases de données seront perdus ou "
"invalidés si le nom d'hôte est modifié."

#. Type: note
#. Description
#: ../templates:10001
msgid "Important changes to nodename (ERLANG_NODE) configuration"
msgstr ""
"Modifications importantes de la configuration du nom de nœud (ERLANG_NODE)"

#. Type: note
#. Description
#: ../templates:10001
msgid ""
"The nodename has changed to reflect ejabberd's upstream recommended nodename "
"configuration (ejabberd@localhost) which saves effort when moving XMPP "
"domains to a different machine."
msgstr ""
"Le nom de nœud a été modifié pour refléter la configuration recommandée par "
"les développeurs amont d'ejabberd (ejabberd@localhost), ce qui évite des "
"efforts lorsque les domaines XMPP sont déplacés sur une machine différente."

#. Type: note
#. Description
#: ../templates:10001
msgid ""
"This may break the current installation but may easily be fixed by editing "
"the ERLANG_NODE option in /etc/default/ejabberd either back to ejabberd or "
"to the name it was manually specified."
msgstr ""
"Cela peut casser l'installation actuelle, mais peut être facilement corrigé "
"en modifiant l'option ERLANG_NODE dans /etc/default/ejabberd soit en "
"revenant à ejabberd soit au nom défini manuellement."

#. Type: note
#. Description
#: ../templates:10001
msgid ""
"Another way to fix a broken installation is to use ejabberdctl's "
"mnesia_change_nodename option to change the nodename in the mnesia database. "
"More information on this method may be found on the ejabberd guide (https://"
"docs.ejabberd.im/admin/guide/managing/#change-computer-hostname). Please "
"make appropriate backups of the database before attempting this method."
msgstr ""
"Un autre moyen de corriger une installation cassée est d'utiliser l'option "
"« mnesia_change_nodename » d'ejabberdctl pour modifier le nom de nœud dans "
"la base de données mnesia. Plus d'informations sur cette méthode se trouvent "
"dans le guide d'ejabberd (https://docs.ejabberd.im/admin/guide/managing/"
"#change-computer-hostname). Veuillez faire les sauvegardes appropriées de la "
"base de données avant d'essayer cette méthode."

# Note to translators:
# Please do not translate the variables ${hostname}, ${user}, ${preseed}, and
# any other which may appear in the future. Changes to these variables will
# break the scripts. Thank you!
#. Type: string
#. Description
#: ../templates:11001
msgid "ERL_OPTIONS for this ejabberd server:"
msgstr "ERL_OPTIONS pour ce serveur ejabberd :"

#. Type: string
#. Description
#: ../templates:11001
msgid ""
"To run the ejabberd server with customized Erlang options, enter them here. "
"It is also possible to set them by editing /etc/ejabberd/ejabberdctl.cfg. "
"See the erl(1) man page for more information."
msgstr ""
"Pour exécuter le serveur ejabberd avec des options personnalisées d'Erlang, "
"veuillez les saisir ici. Il est aussi possible de les définir en éditant le "
"fichier /etc/ejabberd/ejabberdctl.cfg. Consultez la page de manuel erl(1) "
"pour plus d'informations."
